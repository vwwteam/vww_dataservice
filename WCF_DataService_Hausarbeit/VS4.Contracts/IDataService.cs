﻿using System;
using System.ServiceModel;

namespace VS4.Contracts
{
    // Interface, das Clients nutzen, um am Server Funktionen aufzurufen
    [ServiceContract(CallbackContract = typeof(IDataServiceCallBack), SessionMode = SessionMode.Required)]
    public interface IDataServices
    {
        // [DEV] Das scheint auch keinen Unterschied zu machen.
        // Evtl. nicht möglich mit duplex? ..würde keinen Sinn machen
        [OperationContract(IsInitiating = true, IsTerminating = false)]
        User[] Auth(User u);

        [OperationContract]
        void Order(User user, string message);

        [OperationContract]
        void ReturnOrder(OrderEventArgs e);

    }

    // Interface, das es dem Server ermöglicht, Funktionen am Client aufzurufen
    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface IDataServiceCallBack
    {

        [System.ServiceModel.OperationContractAttribute(IsOneWay = true)]
        void ReceiveOrder(OrderEventArgs e);

        [System.ServiceModel.OperationContractAttribute(IsOneWay = true)]
        void ReceiveResult(OrderEventArgs e);


    }

}
