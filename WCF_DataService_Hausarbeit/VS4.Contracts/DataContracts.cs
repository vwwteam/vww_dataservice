﻿using System;
using System.Runtime.Serialization;

namespace VS4.Contracts
{
    [DataContract]
    public class User
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string PublicName { get; set; }

    }

    // Art der Nachricht
    public enum MessageType { ReceiveResult, UserEnter, UserLeave, ReceiveOrder };

    public class OrderEventArgs : EventArgs
    {
        // Der Nachrichtentyp. Dient als Schlüssel, um das entsprechende Callback zu bestimmen
        public MessageType msgType;

        // Das User Objekt der Quelle
        public User sourceUser;

        // Die Quelle der Anfrage
        public string source;

        // Das Ziel der Anfrage bzw. der Antwort beim Rücktransfer
        public string target;

        // Personenschlüssel (teilnehmerübergreifend)
        public string order;

        // Die Tabellen, die für den Anfragenden Nutzer am Zielclient zur Verfügung stehen
        public string tableList;

        // Ein XML-String, der die Antwort auf eine Anfrage beinhaltet
        public string result;
    }
}
