﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using VS4.Contracts;

namespace VS4.Server
{
    class Server
    {

        static void Main(string[] args)
        {
            var svcHost = new ServiceHost(typeof(DataService));
            svcHost.Open();
            Console.WriteLine("Available Endpoints :");
            svcHost.Description.Endpoints.ToList().ForEach(endpoint => Console.WriteLine(endpoint.Address.ToString()));
            Console.WriteLine("\n------------------------------------------");
            Console.ReadLine();
            svcHost.Abort();
            svcHost.Close();

            
        }
    }


    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)] // PerSession, hallo ?
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)] // Nicht sicher, ob es sich überhaupt auswirkt.. warum eigentlich nicht?
    //[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)] // Es macht einfach keinen Unterschied.
    [ServiceBehavior(
                    InstanceContextMode = InstanceContextMode.PerCall,
                    ConcurrencyMode = ConcurrencyMode.Multiple,
                    UseSynchronizationContext = true)] // ..keinen Unterschied
    public class DataService : IDataServices
    {
        public IDataServiceCallBack Proxy
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IDataServiceCallBack>();
            }
        }


        // Thread sync lock Objekt
        private static Object syncObj = new Object();

        // Callback Interface für den Client
        IDataServiceCallBack callback = null;
        public delegate void BroadcastEventHandler(object sender, OrderEventArgs e);

        public static event BroadcastEventHandler ChatEvent;
        private BroadcastEventHandler myEventHandler = null;

        // Enthält eine Liste von Teilnehmern und einen entsprechenden Delegaten,
        // um Nachrichten an alle Teilnehmer senden zu können
        static Dictionary<User, BroadcastEventHandler> chatters = new Dictionary<User, BroadcastEventHandler>();

        private User user;

        /// <summary>
        /// Sucht unter allen verbundenen Teilnehmern nach einem bestimmen
        /// und gibt dessen Delegaten zurück
        /// </summary>
        private BroadcastEventHandler getPersonHandler(string name)
        {
            foreach (User p in chatters.Keys)
            {

                if (p.PublicName.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    BroadcastEventHandler chatTo = null;
                    chatters.TryGetValue(p, out chatTo);
                    return chatTo;
                }
            }
            return null;
        }

        public User[] Auth(User user)
        {
            // Connected zur Datenbank des Servers und überprüft die Logindaten
            var dir = Path.GetDirectoryName(this.GetType().Assembly.Location);
            string directory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString();
            string dbString = string.Format(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={0}\dbServer.mdf;Integrated Security=True", dir);
            SqlConnection sqlCon = new SqlConnection(dbString);
            try
            {
                if (sqlCon.State == ConnectionState.Closed)
                    sqlCon.Open();
                String query = "SELECT COUNT(1) FROM Users WHERE Username=@Username AND Password=@Password";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.CommandType = CommandType.Text;

                sqlCmd.Parameters.AddWithValue("@Username", user.Username);
                sqlCmd.Parameters.AddWithValue("@Password", user.Password);
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                if (count == 1)
                {
                    // Login erfolgreich
                    bool userAdded = false;

                    // Erstellt einen neuen Delegaten, der auf MyEventHandler() zeigt
                    myEventHandler = new BroadcastEventHandler(MyEventHandler);

                    // Prüft, ob ein User übergeben wurde und fügt ihn der Liste der aktiven Teilnehmer
                    // mit dem user als Key und dem EventHandler als Value
                    lock (syncObj)
                    {
                        if (user != null)
                        {
                            this.user = user;
                            chatters.Add(user, MyEventHandler);
                            userAdded = true;

                            Console.WriteLine("\nNew auth request from: " + this.user.PublicName);
                        }
                    }


                    // Sofern ein neuer Nutzer erfolgreich beigetreten ist,
                    // wird ihm eine Liste der verfügbaren Teilnehmer zurückgegeben
                    if (userAdded)
                    {
                        callback = OperationContext.Current.GetCallbackChannel<IDataServiceCallBack>();
                        OrderEventArgs e = new OrderEventArgs();
                        e.msgType = MessageType.UserEnter;


                        // Fügt den Delegaten des neuen Teilnehmers zum Pool der Aktiven Nutzer hinzu
                        ChatEvent += myEventHandler;
                        User[] list = new User[chatters.Count]; // hehe

                        // Kopiert alle Teilnehmer in eine neue Liste
                        lock (syncObj)
                        {
                            chatters.Keys.CopyTo(list, 0);
                        }

                        // [DEV] Hier ist this.user verfügbar, was ja auch Sinn macht. Warum dann nicht in der Order-Function?
                        Console.WriteLine("\nAuth successful " + this.user.PublicName);

                        return list;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    // Logindaten nicht gefunden
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                sqlCon.Close();
            }




        }

        /// <summary>
        /// Sendet eine bestimmte Anfrage/Nachricht an einen bestimmten Teilnehmer
        /// </summary>
        public void Order(User user, string order)
        {
            OrderEventArgs e = new OrderEventArgs();
            e.msgType = MessageType.ReceiveOrder;
            e.sourceUser = user;
            e.source = user.PublicName;
            e.order = order;

            Console.WriteLine("\n------------------------------------------\n");
            Console.WriteLine("New Order from " + user.PublicName + ": " + order + "\n");

            BroadcastOrder(e);
        }


        public void ReturnOrder(OrderEventArgs e)
        {
            string newSource = e.target;
            string newTarget = e.source;
            e.msgType = MessageType.ReceiveResult;
            e.target = newTarget;
            e.source = newSource;

            try
            {
                BroadcastEventHandler resultTo = null;

                // Prüft, ob der User angegebene User (to) verfügbar ist
                lock (syncObj)
                {
                    resultTo = getPersonHandler(e.target);
                    if (resultTo == null)
                    {
                        Console.WriteLine("Error: User not found.");
                        throw new KeyNotFoundException("The person whos name is " + e.target + " could not be found");
                    }
                }
                Console.WriteLine("\n------------------------------------------\n");
                Console.WriteLine("New Result from " + e.source + " to " + e.target);

                // Callt den gewünschten User asynchron (MyEventHandler() und EndAsync())
                resultTo.BeginInvoke(this, e, new AsyncCallback(EndAsync), null);
                Console.WriteLine("Result transfered.");
            }
            catch (KeyNotFoundException)
            {
            }

        }



        /// <summary>
        /// Sendet eine bestimmte Order an alle (anderen) Teilnehmer,
        /// die einer der Nutzergruppen des Users kennen
        /// </summary>
        private void BroadcastOrder(OrderEventArgs e)
        {
            int targetCount = 0;
            Dictionary<int, OrderEventArgs> argList = new Dictionary<int, OrderEventArgs>(); // Hihi
            foreach (User p in chatters.Keys)
            {
                if (!p.PublicName.Equals(e.source, StringComparison.OrdinalIgnoreCase))
                {
                    string userGroups = null;
                    string tableSet = null;
                    lock (syncObj)
                    {
                        e.target = p.PublicName;
                        targetCount++;
                        argList.Add(targetCount, e);
                        Console.WriteLine("Broadcast to: " + argList[targetCount].target);
                    }

                    string directory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString();
                    string dbString = string.Format(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|dbServer.mdf;Integrated Security=True", directory);
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(dbString))
                        {
                            connection.Open();

                            // Holt sich die Nutzergruppe(n) des anfragenden Users
                            using (SqlCommand command = new SqlCommand("SELECT Usergroup FROM Users WHERE Username = '" + argList[targetCount].sourceUser.Username + "'", connection))
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        for (int i = 0; i < reader.FieldCount; i++)
                                        {
                                            // Kommaseparierter String der Nutzergruppen des anfragenden Users
                                            userGroups = "" + reader.GetValue(i) + "";
                                        }
                                    }
                                }
                            }

                            string[] userGroupArr = userGroups.Split(',');
                            foreach (string userGroup in userGroupArr)
                            {
                                // Für jeden (anderen) bekannten Teilnehmer
                                // wird für jede Nutzergruppe des anfragenden Users
                                // nach einem Eintrag in 'TableGroups' gesucht
                                using (SqlCommand command = new SqlCommand("SELECT TableList FROM TableGroups WHERE Owner = '" + argList[targetCount].target + "' AND UserGroup LIKE '%" + userGroup + "%'", connection))
                                {
                                    using (SqlDataReader reader = command.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            for (int i = 0; i < reader.FieldCount; i++)
                                            {
                                                // Jeder gefundene Eintrag enthält freigegebene Tabellennamen am Zielclient
                                                tableSet = "" + reader.GetValue(i) + "";
                                            }
                                        }
                                    }
                                }
                            }

                            argList[targetCount].tableList = tableSet;

                            
                            if (argList[targetCount].tableList != null)
                            {
                                var newI = targetCount;
                                var tmp = argList[newI];
                                BroadcastEventHandler connectTo = null;
                                chatters.TryGetValue(p, out connectTo);
                                connectTo.BeginInvoke(this, tmp, new AsyncCallback(EndAsync), null); // ¯\_(ツ)_/¯
                                //Console.WriteLine("BroadcastOrder END e.target : " + argList[targetCount].target + " count: " + targetCount);

                                // Ich hasse es, aber konnte das Problem nicht anders lösen
                                // und habe alles versucht, um die Instanzen sessionabhängig zu machen. Gnarf!
                                // Ohne timeout werden die falschen EventArgs beim Invoke übergeben
                                // [UPDATE] ..habs nicht hinbekommen..
                                System.Threading.Thread.Sleep(100);

                            }
                            //Console.WriteLine(tableSet);

                            connection.Close();
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {

                    }


                }
            }


        }

        /// <summary>
        /// Wertet den Nachrichtentyp aus und ruft einen entsprechenden Callback auf    
        /// </summary>
        private void MyEventHandler(object sender, OrderEventArgs e)
        {
            try
            {
                switch (e.msgType)
                {
                    case MessageType.ReceiveOrder:
                        callback.ReceiveOrder(e);
                        break;
                    case MessageType.ReceiveResult:
                        callback.ReceiveResult(e);
                        break;
                }
            }
            catch
            {
                
            }
        }

        /// <summary>
        /// Callback des asynchronen Aufrufs. Holt sich den Delegaten und ruft EndInvoke auf
        /// </summary>
        private void EndAsync(IAsyncResult ar)
        {
            BroadcastEventHandler d = null;

            try
            {
                System.Runtime.Remoting.Messaging.AsyncResult asres = (System.Runtime.Remoting.Messaging.AsyncResult)ar; // ¯\_(ツ)_/¯
                d = ((BroadcastEventHandler)asres.AsyncDelegate); // Holt sich den Delegaten
                d.EndInvoke(ar);
            }
            catch
            {
                ChatEvent -= d;
            }
        }
    }
}
