﻿using System;
using System.ServiceModel;
using VS4.Contracts;

// Dieser Client tut momentan nichts weiter, als sich anzumelden
namespace VS4.Krankenhaus
{
    public class Krankenhaus
    {

        public static IDataServices Proxy
        {
            get
            {
                var ctx = new InstanceContext(new DataServiceCallBack());
                return new DuplexChannelFactory<IDataServices>(ctx, "NetTcpBinding_IDataServices").CreateChannel();
            }
        }

        static void Main(string[] args)
        {
            User u = new User()
            {
                Username = "krankenhaus",
                Password = "krankenhaus",
                PublicName = "Krankenhaus"
            };

            


            

            // Momentan gibt Auth() eine Liste von aktiven/angemeldeten Usern zurück,
            // die aber nicht genutzt wird
            User[] userList = Proxy.Auth(u);

            // (dev) Wait for go

            Console.WriteLine("Wartet auf Anfrage..");


            // User Input
            string line;
            line = Console.ReadLine();



            Console.ReadLine();
        }


    }

}
