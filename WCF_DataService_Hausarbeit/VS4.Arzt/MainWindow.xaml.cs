﻿using System.ServiceModel;
using System.Windows;
using VS4.Contracts;


namespace VS4.Arzt
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public User user;
        public MainWindow(LoginScreen screen)
        {
            user = screen.u;
            InitializeComponent();
        }

        public static IDataServices Proxy
        {
            get
            {
                var ctx = new InstanceContext(new DataServiceCallBack());
                return new DuplexChannelFactory<IDataServices>(ctx, "NetTcpBinding_IDataServices").CreateChannel();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataServiceCallBack.xmlData = "";
            string order = personKey.Text;
            Proxy.Order(user, order);
        }

        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {
            //do something
        }
    }

    
}
