﻿using System;
using System.ServiceModel;
using System.Windows;
using VS4.Contracts;

namespace VS4.Arzt
{
    /// <summary>
    /// Interaktionslogik für LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        public static Application WinApp { get; private set; }
        public static MainWindow Window { get; private set; }
        public User u = new User();
        public LoginScreen()
        {
            InitializeComponent();
        }

        public static IDataServices Proxy
        {
            get
            {
                var ctx = new InstanceContext(new DataServiceCallBack());
                return new DuplexChannelFactory<IDataServices>(ctx, "NetTcpBinding_IDataServices").CreateChannel();
            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(txtUsername.Text);
            Console.WriteLine(txtPassword.Password);
            u.Username = txtUsername.Text;
            u.Password = txtPassword.Password;
            u.PublicName = "Arzt";
            

            // Momentan gibt Auth() eine Liste von aktiven/angemeldeten Usern zurück,
            // die aber nicht genutzt wird
            User[] list = Proxy.Auth(u);

            if (list != null)
            {
                foreach (User user in list)
                {
                    // Für jeden User wird ein Button erstellt,
                    // der bei Klick eine Übersicht verfügbarer Datensätze des Users zeigt
                    System.Console.WriteLine(user.PublicName);
                }
                MainWindow dashboard = new MainWindow(this);
                dashboard.Show();
                this.Close();
            }
            else
            {
                Console.WriteLine("Error: Falsches Passwort.");
            }

        }
    }
}
