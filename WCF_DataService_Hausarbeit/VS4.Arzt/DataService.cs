﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using VS4.Contracts;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Windows;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Windows.Controls;

namespace VS4.Arzt
{

    
    public class DataServiceCallBack : IDataServiceCallBack
    {
        public static MainWindow Window { get; private set; }

        public static IDataServices Proxy
        {
            get
            {
                var ctx = new InstanceContext(new DataServiceCallBack());
                return new DuplexChannelFactory<IDataServices>(ctx, "NetTcpBinding_IDataServices").CreateChannel();
            }
        }

        public static string xmlData = "";

        public void ReceiveOrder(OrderEventArgs e)
        {
            Console.WriteLine("\n------------------------------------------\n");
            Console.WriteLine("New order " + e.order + " for Tables: " + e.tableList + "\n");
            // Nimmt die Daten einer Person aus allen freigegebenen Tabellen auf
            Dictionary<string, string> tableSet = new Dictionary<string, string>();

            var dir = Path.GetDirectoryName(this.GetType().Assembly.Location);
            string directory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString();
            string dbString = string.Format(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|db" + e.target + ".mdf;Integrated Security=True", dir);
            string xmlString = "";
            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                // Fügt den Personenschlüssel als Eintrag hinzu
                tableSet.Add("PersonKey", e.order);

                // Teilt die freigegebenen Tabellen auf
                string[] tableList = e.tableList.Split(',');

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    using (SqlConnection connection = new SqlConnection(dbString))
                    {
                        foreach (string table in tableList)
                        {
                            // Sucht alle Einträge aus den freigegebenen Tabellen heraus,
                            // die dem eingegebenen Personenschlüssel zugeordnet werden können
                            using (SqlCommand command = new SqlCommand("SELECT * FROM " + table + " WHERE PersonKey = '" + e.order + "'", connection))
                            {
                                da.SelectCommand = command;

                                da.SelectCommand.Connection.Open();
                                da.Fill(ds);
                                da.SelectCommand.Connection.Close();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlString = ToStringAsXml(ds);

                if (xmlString.Length > 20)
                {
                    e.result = xmlString;
                    Proxy.ReturnOrder(e);
                }
                else
                {
                    Console.WriteLine("No Result.");
                }

            }

        }

        public static string ToStringAsXml(DataSet ds)
        {
            StringWriter sw = new StringWriter();
            ds.WriteXml(sw, XmlWriteMode.IgnoreSchema);
            string s = sw.ToString();
            return s;
        }

        public void ReceiveResult(OrderEventArgs e)
        {
            xmlData += e.result;
            xmlData = Regex.Replace(xmlData, @"<\/NewDataSet><NewDataSet>|<\/Table>\s+<Table>|<PersonKey>.*<\/PersonKey>|<Id>.*<\/Id>", "");
            xmlData = Regex.Replace(xmlData, @"<\/Table>\s+<Table>", "");

            //String xmlData = "<NewDataSet><Inhalt><Id>2</Id><Username>bla</Username><Password>hallo</Password></Inhalt><Inhalt><Id>3</Id><Username>irgendwelche Daten</Username><Password>von dbContracts</Password></Inhalt><Inhalt><Id>5</Id><Username>datenbank</Username><Password>content</Password></Inhalt></NewDataSet>";
            Console.WriteLine(xmlData);
            if(xmlData != null)
            {
                System.IO.StringReader reader = new System.IO.StringReader(xmlData);
                System.Data.DataSet ds = new System.Data.DataSet(xmlData);
                ds.ReadXml(reader);
            }
            

            foreach (Window window in Application.Current.Windows)
            {
                if (window.GetType() == typeof(MainWindow))
                {
                    (window as MainWindow).listBox1.Items.Clear();

                    XDocument ListBoxOptions = XDocument.Parse(xmlData);
                    foreach (XElement element in ListBoxOptions.Root.Elements())
                    {
                            foreach (XElement subelement in element.Elements())
                            {
                            TextBlock item = new TextBlock();
                            item.Text = subelement.Name.ToString();
                            item.FontWeight = FontWeights.Heavy;

                            (window as MainWindow).listBox1.Items.Add(item);
                            (window as MainWindow).listBox1.Items.Add(subelement.Value.ToString());
                           
                        }
                    }
                }
            }
        }

    }
}
