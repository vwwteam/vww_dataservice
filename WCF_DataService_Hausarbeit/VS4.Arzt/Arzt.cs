﻿using System;
using System.Windows;

namespace VS4.Arzt
{
    public class Arzt
    {
        public static Application WinApp { get; private set; }
        public static LoginScreen Window { get; private set; }


        static void InitializeWindows()
        {
            WinApp = new Application();
            WinApp.Run(Window = new LoginScreen()); // note: blocking call
        }

        [STAThread]
        static void Main(string[] args)
        {
            InitializeWindows();
            // Wartet auf Input des Nutzers und vergleicht die Eingabe
            // mit den möglichen Aktionen am Server
            //getUserInput(u);

            Console.ReadLine();
        }

       
    }
}
