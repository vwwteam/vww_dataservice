﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using VS4.Contracts;
using System.Data.SqlClient;
using System.IO;
using System.Data;

namespace VS4.Rathaus
{
    // Client Class

    //[CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DataServiceCallBack : IDataServiceCallBack
    {

        public static IDataServices Proxy
        {
            get
            {
                var ctx = new InstanceContext(new DataServiceCallBack());
                return new DuplexChannelFactory<IDataServices>(ctx, "NetTcpBinding_IDataServices").CreateChannel();
            }
        }


        public void ReceiveOrder(OrderEventArgs e)
        {
            Console.WriteLine("\n------------------------------------------\n");
            Console.WriteLine("New order " + e.order + " for Tables: " + e.tableList + "\n");
            // Nimmt die Daten einer Person aus allen freigegebenen Tabellen auf
            Dictionary<string, string> tableSet = new Dictionary<string, string>();

            // Es tut mir leid
            var dir = Path.GetDirectoryName(this.GetType().Assembly.Location);
            string directory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString();
            string dbString = string.Format(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|db" + e.target + ".mdf;Integrated Security=True", dir);
            string xmlString = "";
            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                // Fügt den Personenschlüssel als Eintrag hinzu
                tableSet.Add("PersonKey", e.order);

                // Teilt die freigegebenen Tabellen auf
                string[] tableList = e.tableList.Split(',');

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    using (SqlConnection connection = new SqlConnection(dbString))
                    {
                        foreach (string table in tableList)
                        {
                            // Sucht alle Einträge aus den freigegebenen Tabellen heraus,
                            // die dem eingegebenen Personenschlüssel zugeordnet werden können
                            using (SqlCommand command = new SqlCommand("SELECT * FROM " + table + " WHERE PersonKey = '" + e.order + "'", connection))
                            {
                                da.SelectCommand = command;

                                da.SelectCommand.Connection.Open();
                                da.Fill(ds);
                                da.SelectCommand.Connection.Close();
                            }
                        }
                    }
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                xmlString = ToStringAsXml(ds);

                if (xmlString.Length > 20)
                {
                    e.result = xmlString;
                    Console.WriteLine(xmlString);
                    Proxy.ReturnOrder(e);
                }
                else
                {
                    Console.WriteLine("No Result.");
                }

            }

        }

        public static string ToStringAsXml(DataSet ds)
        {
            StringWriter sw = new StringWriter();
            ds.WriteXml(sw, XmlWriteMode.IgnoreSchema);
            string s = sw.ToString();
            return s;
        }

        public void ReceiveResult(OrderEventArgs e)
        {
            Console.WriteLine(e.result);
        }

    }
}
